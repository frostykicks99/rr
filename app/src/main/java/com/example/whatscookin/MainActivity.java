package com.example.whatscookin;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;


import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;


public class MainActivity extends AppCompatActivity {

    GridLayout gridLayout;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridLayout = (GridLayout) findViewById(R.id.mainGrid);

        setSingleEvent(gridLayout);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    // we are setting onClickListener for each element
    private void setSingleEvent(GridLayout gridLayout) {
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            CardView cardView = (CardView) gridLayout.getChildAt(i);
            final int finalI = i;


            cardView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (finalI == 0) {
                        Toast.makeText(MainActivity.this, "Chicken selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, ChickenActivity.class);
                        startActivity(intent);
                    } else if (finalI == 1) {
                        Toast.makeText(MainActivity.this, "Beef Selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, BeefActivity.class);
                        startActivity(intent);
                    } else if (finalI == 2)  {
                        Toast.makeText(MainActivity.this, "Vegetarian Selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, VegetarianActivity.class);
                        startActivity(intent);
                    } else if (finalI == 3)  {
                        Toast.makeText(MainActivity.this, "Seafood Selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, SeafoodActivity.class);
                        startActivity(intent);
                    } else if (finalI == 4)  {
                        Toast.makeText(MainActivity.this, "Pork Selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, PorkActivity.class);
                        startActivity(intent);
                    } else if (finalI == 5)  {
                        Toast.makeText(MainActivity.this, "Surprise Me Selected",
                                Toast.LENGTH_SHORT).show();
                        intent = new Intent(MainActivity.this, SurpriseActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }


    }
}