package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class BeefActivity extends Activity {

    Random r = new Random();

    String[] items = new String[] {
            "BEEF AND BROCCOLI\n\n" +
                    "INGREDIENTS\n" +
                    "3 Tablespoons cornstarch, divided\n" +
                    "1 pound flank steak, cut into thin 1-inch pieces\n" +
                    "1/2 cup low sodium soy sauce\n" +
                    "3 Tablespoons packed light brown sugar\n" +
                    "1 Tablespoon minced garlic\n" +
                    "2 teaspoons grated fresh ginger\n" +
                    "2 Tablespoons vegetable oil, divided\n" +
                    "4 cups small broccoli florets\n" +
                    "1/2 cup sliced white onions\n\n" +
                    "INSTRUCTIONS\n" +
                    "In a bowl, mix 2 tablespoons cornstarch with 3 tablespoons water. Add the beef to the bowl and toss to combine.\n" +
                    "\n" +
                    "In a separate bowl, mix the remaining 1 tablespoon cornstarch with the soy sauce, brown sugar, garlic and ginger. Set the sauce aside.\n" +
                    "\n" +
                    "Heat a large sauté pan over medium heat. Add 1 tablespoon of the vegetable oil and once it is hot, add the beef, stirring constantly until the beef is almost cooked through. Using a slotted spoon, transfer the beef to a plate and set it aside.\n" +
                    "\n" +
                    "Add the remaining 1 tablespoon of vegetable oil to the pan and once it is hot, add the broccoli florets and sliced onions and cook, stirring occasionally, until the broccoli is tender, about 4 minutes. (See Notes.)\n" +
                    "\n" +
                    "Return the beef to the pan then add the prepared sauce. Bring the mixture to a boil and cook, stirring, for 1 minute or until the sauce thickens slightly. Serve with rice or noodles.\n" +
                    "\n\n" +
                    "NOTES:\n" +
                    "To guarantee bright green broccoli, blanch the florets in boiling water then drain and dry it very well before adding it to the pan. If you opt for this additional blanching step, reduce the cooking time of the broccoli to 2 minutes.\n" +
                    "\n" +
                    "The sauce must come to a boil in order for the cornstarch to thicken.",

            "BEEF BULGOGI STIR-FRY\n\n" +
                    "INGREDIENTS\n" +
                    "4 cm or 1 ½ in piece of ginger\n" +
                    "4 tbsp soy sauce\n" +
                    "4 tbsp mirin\n" +
                    "3 garlic cloves\n" +
                    "2 tbsp chopped pineapple\n" +
                    "2 tsp red chilli flakes or Korean chilli powder\n" +
                    "3 tbsp golden caster sugar\n" +
                    "3 tsp sesame oil\n" +
                    "500g sirloin or rump steak, trimmed of fat and sliced\n" +
                    "1 large onion, cut into half moons\n" +
                    "1 tbsp toasted sesame seeds\n" +
                    "200g cooked basmati rice\n" +
                    "chopped spring onions, to serve\n\n" +
                    "INSTRUCTIONS\n" +
                    "Put the ginger, soy, mirin, garlic, pineapple, chilli flakes, sugar and 1 tsp of the sesame oil in a food processor and blend until fine. Pour the marinade into a bowl, add the meat, mix well and leave to sit while you prepare the onion.\n" +
                    "\n" +
                    "Heat the remaining sesame oil in a large wok or frying pan until very hot. Add the onion and stir-fry for a few mins. Add the beef and the marinade, stirring constantly until it’s cooked through, about 5 mins. Sprinkle with the sesame seeds and serve with rice and chopped spring onions.",

            "SHEPHERD'S PIE\n\n" +
                    "INGREDIENTS\n" +
                    "1 1/2 pounds ground beef\n" +
                    "1 medium onion, chopped\n" +
                    "2 garlic cloves, minced\n" +
                    "1 envelope taco seasoning\n" +
                    "2 cups shredded cheddar cheese, divided\n" +
                    "3 cups mashed potatoes, warmed\n" +
                    "1/2 cup water\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat broiler. In a large ovenproof skillet, cook beef, onion and garlic over medium heat until beef is no longer pink, breaking up beef into crumbles; drain.\n\n" +
                    "Stir in water and taco seasoning; heat through. Stir in 1 cup cheese. Remove from heat.\n\n" +
                    "In a bowl, mix mashed potatoes and remaining cheese; spread over beef mixture.\n\n" +
                    "Broil 4-6 in. from heat 5-6 minutes or until top is golden brown.",

            "STEAK FAJITAS\n\n" +
                    "INGREDIENTS\n" +
                    "2 pounds skirt, flank, sirloin, or hanger steak, sliced into 1/2 inch strips\n" +
                    "1 red pepper, sliced into thin strips\n" +
                    "1 green or yellow pepper, sliced into thin strips\n" +
                    "1 medium onion, sliced into thin strips\n" +
                    "3 tablespoons olive oil\n" +
                    "1 tablespoon lime juice\n" +
                    "1/2 teaspoon chili powder\n" +
                    "1 teaspoon ground cumin\n" +
                    "pinch cayenne pepper\n" +
                    "1/2 teaspoon Kosher salt\n" +
                    "1/2 teaspoon fresh ground black pepper\n" +
                    "2 cloves garlic, minced\n" +
                    "6-8 tortillas\n\n" +
                    "INSTRUCTIONS\n" +
                    "Place the steak into a zipper bag. Place the peppers and onion into a separate zipper bag. Add the olive oil, lime juice, chili powder, ground cumin, cayenne pepper, salt, black pepper, and garlic to a jar with a screw top or tight fitting lid. Shake until well combined.\n\n" +
                    "Pour 1/3 of the marinade over steak, 1/3 of marinade over vegetables and reserve the remaining marinade in the jar for using when cooking the steak fajitas. Seal the bags tightly and refrigerate for one hour to overnight.\n\n" +
                    "When ready to cook, heat a large skillet over medium-high heat. Pour the vegetables into the skillet and cook until just tender crisp, about 5 minutes. Remove the vegetables from the skillet to a plate and add steak strips to the same skillet.\n\n" +
                    "When the steak is cooked throughout (about 7-10 minutes), add vegetables back to the skillet along with the reserved marinade.\n\n" +
                    "Serve with heated tortillas."

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beef);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items [r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}