package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class VegetarianActivity extends Activity {

    Random r = new Random();

    String[] items = new String[] {
            "BAKED EGGPLANT PARMESAN\n\n" +
                    "INGREDIENTS\n" +
                    "1 1/2 cup panko bread crumbs\n" +
                    "1 cup freshly grated Parmesan, divided\n" +
                    "2 tsp. Italian seasoning\n" +
                    "Kosher salt\n" +
                    "Freshly ground black pepper\n" +
                    "2 medium eggplants, sliced into 1/2 inch thick rounds\n" +
                    "3 large eggs\n" +
                    "4 cups marinara\n" +
                    "2 cups shredded mozzarella\n" +
                    "1/3 cup thinly sliced basil\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 425°. Line two large baking sheets with parchment paper and coat with cooking spray. In a shallow bowl, whisk together panko, 1/2 cup " +
                    "\n" +
                    "Parmesan, and Italian seasoning. Season with salt and pepper. In another shallow bowl, whisk eggs with 2 tablespoons water and season with salt and pepper.\n" +
                    "\n" +
                    "Dip an eggplant slice into egg wash, then dredge in panko mixture. Place on baking sheet. Repeat to coat all eggplant slices. Spray tops lightly with cooking spray.\n" +
                    "Bake until soft inside, and golden and crisp on the outside, 30 to 35 minutes.\n" +
                    "\n" +
                    "In a large baking dish, add a cup of marinara and spread evenly. Add an even layer of eggplant slices, then pour 1 1/2 cup marinara on top. Sprinkle with 1 cup mozzarella, half of the remaining Parmesan, and fresh basil. " +
                    "\n" +
                    "Repeat process once more to use up all ingredients.\n" +
                    "\n" +
                    "Bake until bubbly and golden, 15 to 17 minutes more.",

            "PORTOBELLO MUSHROOM BURGER\n\n" +
                    "INGREDIENTS\n" +
                    "4 large portobello mushrooms, stems and gills removed\n" +
                    "1/3 cup extra-virgin olive oil\n" +
                    "1 tsp. garlic powder\n" +
                    "1/2 tsp. ground mustard\n" +
                    "1 tbsp. Worcestershire sauce\n" +
                    "4 brioche buns\n\n" +
                    "OPTIONAL FOR SERVING:\n" +
                    "Honey mustard\n" + "Tomato slices\n" + "Lettuce\n\n" +
                    "INSTRUCTIONS\n" +
                    "Preheat oven to 375°. In a medium bowl, whisk together olive oil, garlic powder, mustard powder and Worcestershire.\n" +
                    "\n" +
                    "Dip mushroom caps in the mixture and place mushrooms face-down on a baking sheet.\n" +
                    "\n" +
                    "Sprinkle with salt and pepper, to taste. Bake for 20 minutes, until softened.\n" +
                    "\n" +
                    "Top with desired toppings and serve.\n",

            "AVOCADO SANDWICH\n\n" +
                    "INGREDIENTS\n" +
                    "2 slices of bread, toasted\n" +
                    "4 slices smoked tofu\n" +
                    "salt, to season\n" +
                    "¼ cup healthy greens\n" +
                    "½ avocado, mashed\n" +
                    "2 tomato slices\n" +
                    "Hot sauce, to taste (optional)\n\n" +
                    "INSTRUCTIONS\n" +
                    "Toast 2 slices of bread.\n" +
                    "Slice your avocado, tomato and smoked tofu.\n" +
                    "Season smoked tofu with salt(optional)\n" +
                    "When your bread is toasted, spread your avocado on each slice.\n" +
                    "Next, add your lettuce or sprouts and slice of tomato.\n" +
                    "Season your tomato with salt(optional)\n" +
                    "Add slices of smoked tofu on top.\n" +
                    "Sprinkle hot sauce(optional)",

            "CHILI\n\n" +
                    "INGREDIENTS\n" +
                    "3 cloves Garlic\n" +
                    "1 Small Yellow Onion, diced\n" +
                    "2 Green Bell Peppers, diced\n" +
                    "3 ribs Celery, diced\n" +
                    "3 small Carrots, sliced\n" +
                    "¼ cup Ancho Chili Powder*\n" +
                    "1 tbsp Cumin\n" +
                    "1 tsp Dried Oregano\n" +
                    "½ tsp Cayenne Pepper (Optional)\n" +
                    "2 15 oz cans Red Kidney Beans, drained\n" +
                    "2 15 oz cans Pinto Beans, drained\n" +
                    "2 28 oz cans Crushed Tomatoes\n" +
                    "1 cup Filtered Water\n" +
                    "Salt and Black Pepper, to taste\n" +
                    "Optional Toppings: Green Onions, Nutritional Yeast, Vegan Sour Cream\n\n" +
                    "INSTRUCTIONS\n" +
                    "First add the Onion, Celery, and Green Pepper to a large nonstick pot over Medium-High heat with  ⅓ cup of Water. Cook until all the water evaporates, stirring occasionally. Once the pot is “dry” and the vegetables start to turn golden brown, deglaze the pot with an additional ¼ cup of Water. This process should take around 10 minutes total.\n" +
                    "\n" +
                    "Add the Garlic, Spices, and Carrots to the pot, then sauté for a few minutes, adding a small splash of water if things start to stick.\n" +
                    "\n" +
                    "FInally, add the drained Beans, Crushed Tomatoes, and 1 cup of Filtered Water to the Pot. Bring everything to a boil over high heat, then reduce the heat to a simmer, cover, and cook for 10 minutes. Remove the lid from the pot and cook uncovered for 5-7 additional minutes, until the Carrots are tender and the Chili reaches your desired thickness. Stir this mixture occasionally, to make sure nothing sticks to the bottom of the pot.\n" +
                    "\n" +
                    "Serve warm, and top as desired. Leftovers can be store in the fridge for up to 7 days, or in the freezer for up to one month.",

            "LENTIL SOUP\n\n" +
                    "Ingredients\n" +
                    "1 onion, diced (or sub 2 fat shallots)\n" +
                    "6  garlic cloves chopped\n" +
                    "2 tbsp. olive oil\n" +
                    "4 cups diced veggies (any mix)\n" +
                    "1 1/2  tsp. salt, more to taste.\n" +
                    "1 tbsp. cumin\n" +
                    "2 tsp. coriander\n" +
                    "2 tsp. curry powder\n" +
                    "1 tsp. turmeric\n" +
                    "1 tsp. Italian herbs\n" +
                    "2 tbsp. tomato paste\n" +
                    "2 medium tomatoes, diced with their juices (one 14.5 ounce can, diced)\n" +
                    "4 cups veggie stock (or use 3 bouillon cubes w/ water)\n" +
                    "2 cups water\n" +
                    "1 ¼ cup small Black lentils or Green Lentils\n" +
                    "(You can always use bigger, brown lentils, but increase pressure cooking time to 20 minutes.)\n" +
                    "Finish with olive oil, lemon juice, chopped parsley (or cilantro), diced tomatoes and (optional) chili flakes and toasted pita.\n\n" +
                    "INSTRUCTIONS\n" +
                    "Saute onion and garlic in the Instant Pot in 2 tablespoons oil until fragrant and tender, about 2-3 minutes.  (Or cook in a dutch oven on the stove, over medium-high heat)\n" +
                    "\n" +
                    "Add the veggies, spices and salt. Saute 4-5 more minutes. Add the tomato paste and brown it a bit. Add the tomatoes and their juices, stock and water, scraping up any browned bits. Add the lentils, stir and cover, setting the Instant pot to Pressure Cook on Normal for 12 minutes. (Alternately if cooking on the stove top, bring to a simmer and cover, simmering on low for 20-25 minutes)\n" +
                    "\n" +
                    "Plate and drizzle with olive oil, a squeeze of lemon, chili flakes, fresh parsley and fresh diced tomato (optional)"

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vegetarian);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items[r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}

