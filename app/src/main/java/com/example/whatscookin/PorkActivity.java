package com.example.whatscookin;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class PorkActivity extends Activity {

    Random r = new Random();

    String[] items = new String[]{
            "SPICY PORK\n\n" +
                    "INGREDIENTS\n" +
                    "2 tablespoons hot chili paste\n" +
                    "2 tablespoons soy sauce\n" +
                    "2 tablespoons brown sugar\n" +
                    "1 1-inch piece of ginger, peeled and finely grated\n" +
                    "1 garlic clove, finely grated\n" +
                    "1 1/2 pounds pork tenderloin, very thinly sliced\n" +
                    "vegetable oil\n\n" +
                    "DIRECTIONS\n" +
                    "Shake all the sauce ingredients in a jar. Pour the sauce over the pork and allow it to rest for 20 minutes – 1 hour.\n" +
                    "\n" +
                    "Heat a good searing skillet (such as cast iron) over high heat. Add just a little bit of vegetable oil.\n" +
                    "\n" +
                    "When the oil is hot and shiny, add the pork in a single layer (you may need to do this in batches). Cook, undisturbed, for 1-2 minutes until you are getting that nice caramelized look. Flip and repeat to finish.\n",

            "GARLIC PORK ROAST\n\n" +
                    "INGREDIENTS\n" +
                    "3 lb. boneless pork loin\n" +
                    "3 cloves garlic, minced\n" +
                    "2 tbsp. mustard\n" +
                    "1 tbsp. chopped rosemary\n" +
                    "1 tsp. chopped thyme leaves\n" +
                    "kosher salt\n" +
                    "Freshly ground black pepper\n" +
                    "Fresh rosemary sprigs\n" +
                    "3 tbsp. melted butter\n" +
                    "1 tbsp. brown sugar\n\n" +
                    "DIRECTIONS\n" +
                    "Preheat oven to 400°. Line a pan with foil and place a wire rack on top.\n" +
                    "\n" +
                    "Roll the flap of the boneless loin into a cylinder and using kitchen twine, tie the pork loin every few inches. (This helps cook the pork more evenly.)\n" +
                    "\n" +
                    "In a small bowl, mix together garlic, mustard, rosemary, thyme, 1 1/2 teaspoons salt, and freshly ground black pepper. Rub mixture all over pork loin and place in roasting pan fat-side down. Bake for 30 minutes, then carefully flip the loin and bake until a thermometer inserted into the middle of the meat reads 145°, about 20 minutes.\n" +
                    "\n" +
                    "Mix melted butter and brown sugar together, then brush on top of the pork loin, let caramelize.\n" +
                    "\n" +
                    "Let rest for 10 minutes before slicing.",

            "BLT SANDWICH\n\n" +
                    "INGREDIENTS\n" +
                    "4 slices bacon\n" +
                    "2 leaves lettuce\n" +
                    "2 slices tomato\n" +
                    "2 slices bread, toasted  \n" +
                    "1 tablespoon mayonnaise\n\n" +
                    "DIRECTIONS\n" +
                    "Cook the bacon in a large skillet over medium-high heat until evenly browned, about 10 minutes. Place the bacon slices on a paper towel-lined plate.\n" +
                    "\n" +
                    "Arrange the cooked bacon, lettuce, and tomato slices on one slice of bread. Spread one side of remaining bread slice with the mayonnaise. Put the two pieces together to make a sandwich.",


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pork);

        TextView textView = (TextView) findViewById(R.id.textView);

        textView.setText("" + items[r.nextInt(items.length)]);

        textView.setMovementMethod(new ScrollingMovementMethod());

        ImageButton back1 = (ImageButton) findViewById(R.id.back);
        back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
